import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { AssetsModule } from './assets/assets.module';
import { AssetsCategoriesModule } from './assetsCategories/assetsCategories.module';
import { SpecificFieldsModule } from './specific-fields/specific-fields.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/db_assets'),
    AssetsModule,
    AssetsCategoriesModule,
    SpecificFieldsModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
