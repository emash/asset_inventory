import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SpecificFields } from 'src/specific-fields/interfaces/specific-fields.interface';
import { CreateSpecificFieldsDto } from './dto/specific-fields.dto';
import { AssetsCategoriesService } from '../assetsCategories/assetsCategories.service';

@Injectable()
export class SpecificFieldsService {
  constructor(
    @InjectModel('SpecificFields')
    private readonly specificFieldsModel: Model<SpecificFields>,
    private readonly assetsCategoriesService: AssetsCategoriesService
  ) {}

  async findAll(): Promise<SpecificFields[]> {
    Logger.log(this.findAll.name, SpecificFieldsService.name);
    return await this.specificFieldsModel.find().populate('category');
  }

  async findOne(id: string): Promise<SpecificFields> {
    Logger.log(this.findOne.name, SpecificFieldsService.name);
    return await this.specificFieldsModel.findOne({ _id: id });
  }

  async create(item: CreateSpecificFieldsDto): Promise<SpecificFields> {
    Logger.log(this.create.name, SpecificFieldsService.name);
    const assetCategory = await this.assetsCategoriesService.findOne(
      item.category
    );

    if (assetCategory) {
      const specificFields = new this.specificFieldsModel(item);
      return await specificFields.save();
    } else {
      return null;
    }
  }

  async delete(id: string): Promise<SpecificFields> {
    Logger.log(this.delete.name, SpecificFieldsService.name);
    return await this.specificFieldsModel.findByIdAndRemove(id);
  }

  async update(
    id: string,
    item: CreateSpecificFieldsDto
  ): Promise<SpecificFields> {
    Logger.log(this.delete.name, SpecificFieldsService.name);
    return await this.specificFieldsModel.findByIdAndUpdate(id, item, {
      new: true
    });
  }
}
