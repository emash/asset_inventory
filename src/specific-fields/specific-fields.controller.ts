import {
  Controller,
  Get,
  Post,
  Delete,
  Logger,
  Body,
  Param,
  Put
} from '@nestjs/common';
import { AssetsCategoriesController } from 'src/assetsCategories/assetsCategories.controller';
import { SpecificFieldsService } from './specific-fields.service';
import { CreateSpecificFieldsDto } from './dto/specific-fields.dto';
import { SpecificFields } from 'src/specific-fields/interfaces/specific-fields.interface';

@Controller('specific-fields')
export class SpecificFieldsController {
  constructor(private readonly specificFieldsService: SpecificFieldsService) {}

  @Get()
  async findAll(): Promise<SpecificFields[]> {
    Logger.log(this.findAll.name, SpecificFieldsController.name);
    return await this.specificFieldsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: any): Promise<SpecificFields> {
    Logger.log(this.findOne.name, SpecificFieldsController.name);
    return await this.specificFieldsService.findOne(id);
  }

  @Post()
  async create(
    @Body() createItemDto: CreateSpecificFieldsDto
  ): Promise<SpecificFields> {
    Logger.log(this.create.name, SpecificFieldsController.name);
    return await this.specificFieldsService.create(createItemDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: any): Promise<SpecificFields> {
    Logger.log(this.delete.name, SpecificFieldsController.name);
    return await this.specificFieldsService.delete(id);
  }

  @Put(':id')
  async update(
    @Body() updateItemDto: CreateSpecificFieldsDto,
    @Param('id') id
  ): Promise<SpecificFields> {
    Logger.log(this.update.name, SpecificFieldsController.name);
    return await this.specificFieldsService.update(id, updateItemDto);
  }
}
