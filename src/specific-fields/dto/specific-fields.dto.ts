import { IsNotEmpty, IsMongoId } from 'class-validator';

export class CreateSpecificFieldsDto {
  @IsNotEmpty()
  readonly name: string;

  @IsMongoId()
  readonly category: string;

  @IsNotEmpty()
  readonly fieldType: string;

  @IsNotEmpty()
  readonly required: string;
}
