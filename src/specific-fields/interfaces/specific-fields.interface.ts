import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export interface SpecificFields extends Document {
  name: string;
  category: mongoose.Schema.Types.ObjectId;
  fieldType: string;
  required: boolean;
  values: string[];
  minValue: number;
  maxValue: number;
  defaultValue: string;
}
