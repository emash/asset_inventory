import * as mongoose from 'mongoose';

export const SpecificFieldsSchema = new mongoose.Schema({
  _id: {
    type: mongoose.SchemaTypes.ObjectId,
    auto: true
  },
  name: {
    type: String,
    required: true
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'AssetCategory'
  },
  fieldType: {
    type: String,
    enum: ['String', 'Integer', 'List', 'Boolean']
  },
  values: {
    type: Array
  },
  minValue: {
    type: Number
  },
  maxValue: {
    type: Number
  },
  defaultValue: {
    type: String
  },
  required: {
    type: Boolean,
    required: true
  }
});
