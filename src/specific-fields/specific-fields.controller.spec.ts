import { Test, TestingModule } from '@nestjs/testing';
import { SpecificFieldsController } from './specific-fields.controller';

describe('SpecificFields Controller', () => {
  let controller: SpecificFieldsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SpecificFieldsController],
    }).compile();

    controller = module.get<SpecificFieldsController>(SpecificFieldsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
