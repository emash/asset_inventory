import { Test, TestingModule } from '@nestjs/testing';
import { SpecificFieldsService } from './specific-fields.service';

describe('SpecificFieldsService', () => {
  let service: SpecificFieldsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SpecificFieldsService],
    }).compile();

    service = module.get<SpecificFieldsService>(SpecificFieldsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
