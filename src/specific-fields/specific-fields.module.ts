import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SpecificFieldsService } from './specific-fields.service';
import { SpecificFieldsController } from './specific-fields.controller';
import { SpecificFieldsSchema } from './schemas/specific-fields.schema';
import { AssetCategorySchema } from '../assetsCategories/schemas/assetCategory.schema';
import { AssetsCategoriesService } from '../assetsCategories/assetsCategories.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'SpecificFields', schema: SpecificFieldsSchema },
      { name: 'AssetCategory', schema: AssetCategorySchema }
    ])
  ],
  controllers: [SpecificFieldsController],
  providers: [SpecificFieldsService, AssetsCategoriesService],
  exports: [SpecificFieldsService]
})
export class SpecificFieldsModule {}
