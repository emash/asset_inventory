
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AssetsController } from './assets.controller';
import { AssetsService } from './assets.service';
import { AssetSchema } from './schemas/asset.schema';
import { AssetCategorySchema } from '../assetsCategories/schemas/assetCategory.schema';
import {AssetsCategoriesService } from '../assetsCategories/assetsCategories.service';


@Module({
	imports: [MongooseModule.forFeature([
		{ name: 'Asset', schema: AssetSchema },
		{ name: 'AssetCategory', schema: AssetCategorySchema }
		])],
	controllers: [AssetsController],
	providers: [AssetsService, AssetsCategoriesService],
})
export class AssetsModule {}
