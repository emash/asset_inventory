import { IsNotEmpty, IsMongoId, IsOptional, IsArray } from 'class-validator';

export class CreateAssetDto {
  @IsNotEmpty()
  readonly name: string;

  @IsMongoId()
  readonly category: string;

  @IsOptional()
  @IsMongoId()
  readonly parentId: string;

  @IsOptional()
  @IsArray()
  readonly childrenId: [string];

  @IsOptional()
  @IsArray()
  readonly tags: [string];
}
