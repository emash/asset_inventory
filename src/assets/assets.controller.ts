import { Controller, Get, Post, Put, Delete, Body, Param, HttpStatus, HttpException, Logger } from '@nestjs/common';
import { CreateAssetDto } from './dto/create-asset.dto';
import { AssetsService } from './assets.service';
import { Asset } from './interfaces/asset.interface';

@Controller('assets')
  export class AssetsController {
    constructor(private readonly assetsService: AssetsService) {}

    @Get()
    async findAll(): Promise<Asset[]> {
      Logger.log(this.findAll.name, AssetsController.name);
      return await this.assetsService.findAll();
    }

    @Get(':id')
    async findOneById(@Param('id') id: any): Promise<Asset> {
      Logger.log(this.findOneById.name, AssetsController.name);
      return await this.assetsService.findOneById(id);
    }

    @Post()
    async create(@Body() createAssetDto: CreateAssetDto): Promise<Asset> {
      Logger.log(this.create.name, AssetsController.name);
      const asset = await this.assetsService.create(createAssetDto);
      if (asset)
        return (asset);
      throw new HttpException('Asset Not created', HttpStatus.NOT_FOUND);
    }

    @Delete(':id')
    async delete(@Param('id') id: any): Promise<Asset> {
      Logger.log(this.delete.name, AssetsController.name);
      return await this.assetsService.delete(id);
    }

    @Put(':id')
    async update(@Body() createAssetDto: CreateAssetDto, @Param('id') id: any): Promise<Asset> {
      Logger.log(this.update.name, AssetsController.name);
      return await this.assetsService.update(id, createAssetDto);
    }
  }
