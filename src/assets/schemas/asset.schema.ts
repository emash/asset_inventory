
import * as mongoose from 'mongoose';

export const AssetSchema = new mongoose.Schema({
  name: {
  	type: String,
  	required: true
  },
  category: {
  	type: mongoose.Schema.Types.ObjectId,
    required: true,
  	ref: 'AssetCategory'
  },
  parentId: {
  	type: mongoose.Schema.Types.ObjectId,
    default: null,
    ref: 'Asset'
  },
  childrenId: [{
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: 'Asset'
  }],
  tags: {
    type: Array,
    required: false,
    ref: 'Asset'
  }]
});
