import { Injectable, Logger } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { CreateAssetDto } from './dto/create-asset.dto';
import { Asset } from './interfaces/asset.interface';
import { AssetsCategoriesService } from '../assetsCategories/assetsCategories.service';

@Injectable()
export class AssetsService {
  constructor(
    @InjectModel('Asset') private readonly assetModel: Model<Asset>,
    private assetsCategoriesService: AssetsCategoriesService
  ) {}

  async findAll(): Promise<Asset[]> {
    Logger.log(this.findAll.name, AssetsService.name);
    return await this.assetModel
      .find()
      .populate('parentId')
      .populate('category')
      .populate('childrenId');
  }

  async findOneById(id: string): Promise<Asset> {
    Logger.log(this.findOneById.name, AssetsService.name);
    return await this.assetModel
      .findById(id)
      .populate('parentId')
      .populate('category')
      .populate('childrenId');
  }

  async create(asset: CreateAssetDto): Promise<Asset> {
    Logger.log(this.create.name, AssetsService.name);
    const newAsset = new this.assetModel(asset);
    newAsset.category = await this.assetsCategoriesService.findOne(
      asset.category
    );

    if (asset.parentId) {
      newAsset.parentId = await this.findOneById(asset.parentId);
    }
    if (!newAsset.category) return null;
    return await newAsset.save();
  }

  async delete(id: string): Promise<Asset> {
    Logger.log(this.delete.name, AssetsService.name);
    return await this.assetModel.findByIdAndRemove(id);
  }

  async update(id: string, asset: CreateAssetDto): Promise<Asset> {
    Logger.log(this.update.name, AssetsService.name);
    return await this.assetModel.findByIdAndUpdate(id, asset, { new: true });
  }
}
