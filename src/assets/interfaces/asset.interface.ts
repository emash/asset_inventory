import * as mongoose from 'mongoose';
import { AssetCategory } from 'src/assetsCategories/interfaces/assetCategory.interface';

export interface Asset extends mongoose.Document {
  name: string;
  category: AssetCategory;
  parentId: Asset;
  childrenId: [Asset];
  tags: string[];
}
