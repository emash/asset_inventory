import { Injectable, Logger } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { CreateAssetCategoryDto } from './dto/create-asset-category.dto';
import { AssetCategory } from './interfaces/assetCategory.interface';

@Injectable()
export class AssetsCategoriesService {
  constructor(
    @InjectModel('AssetCategory')
    private readonly assetCategoryModel: Model<AssetCategory>
  ) {}

  async findAll(): Promise<AssetCategory[]> {
    Logger.log(this.findAll.name, AssetsCategoriesService.name);
    return await this.assetCategoryModel.find();
  }

  async findOne(id: string): Promise<AssetCategory> {
    Logger.log(this.findOne.name, AssetsCategoriesService.name);
    return await this.assetCategoryModel.findOne({ _id: id });
  }

  async create(item: CreateAssetCategoryDto): Promise<AssetCategory> {
    Logger.log(this.create.name, AssetsCategoriesService.name);
    const assetCategory = new this.assetCategoryModel(item);
    return await assetCategory.save();
  }

  async delete(id: string): Promise<AssetCategory> {
    Logger.log(this.delete.name, AssetsCategoriesService.name);
    return await this.assetCategoryModel.findByIdAndRemove(id);
  }

  async update(
    id: string,
    item: CreateAssetCategoryDto
  ): Promise<AssetCategory> {
    Logger.log(this.update.name, AssetsCategoriesService.name);
    return await this.assetCategoryModel.findByIdAndUpdate(id, item, {
      new: true
    });
  }
}
