import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  Logger
} from '@nestjs/common';
import { CreateAssetCategoryDto } from './dto/create-asset-category.dto';
import { AssetsCategoriesService } from './assetsCategories.service';
import { AssetCategory } from './interfaces/assetCategory.interface';

@Controller('categories')
export class AssetsCategoriesController {
  constructor(
    private readonly assetsCategoriesService: AssetsCategoriesService
  ) {}

  @Get()
  async findAll(): Promise<AssetCategory[]> {
    Logger.log(this.findAll.name, AssetsCategoriesController.name);
    return await this.assetsCategoriesService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: any): Promise<AssetCategory> {
    Logger.log(this.findOne.name, AssetsCategoriesController.name);
    return await this.assetsCategoriesService.findOne(id);
  }

  @Post()
  async create(
    @Body() createItemDto: CreateAssetCategoryDto
  ): Promise<AssetCategory> {
    Logger.log(this.create.name, AssetsCategoriesController.name);
    return await this.assetsCategoriesService.create(createItemDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: any): Promise<AssetCategory> {
    Logger.log(this.delete.name, AssetsCategoriesController.name);
    return await this.assetsCategoriesService.delete(id);
  }

  @Put(':id')
  async update(
    @Body() updateItemDto: CreateAssetCategoryDto,
    @Param('id') id
  ): Promise<AssetCategory> {
    Logger.log(this.update.name, AssetsCategoriesController.name);
    return await this.assetsCategoriesService.update(id, updateItemDto);
  }
}
