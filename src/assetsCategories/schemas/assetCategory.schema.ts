
import * as mongoose from 'mongoose';

export const AssetCategorySchema = new mongoose.Schema({
	_id: {
		type: mongoose.SchemaTypes.ObjectId,
		auto: true
	},
	name: String,
});
