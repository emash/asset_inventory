
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AssetsCategoriesController } from './assetsCategories.controller';
import { AssetsCategoriesService } from './assetsCategories.service';
import { AssetCategorySchema } from './schemas/assetCategory.schema';

@Module({
  imports: [
  	MongooseModule.forFeature([{ name: 'AssetCategory', schema: AssetCategorySchema }])
  	],
  controllers: [AssetsCategoriesController],
  providers: [AssetsCategoriesService],
  exports: [AssetsCategoriesService],
})
export class AssetsCategoriesModule {}
