import { IsNotEmpty } from 'class-validator';

export class CreateAssetCategoryDto {

	@IsNotEmpty()
	readonly name: string;
}
