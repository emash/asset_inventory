import { Document } from "mongoose";

export interface AssetCategory extends Document {
  name: string;
}
