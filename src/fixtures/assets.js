const { ObjectID: ObjectId } = require('mongodb');
let fixtureAssetCategory = require('./assetcategories');

const buildingId = new ObjectId(); 
const firstFloorId = new ObjectId();
const wallId = new ObjectId();
const doorId = new ObjectId();

const buildingAsset = {
    _id: buildingId,
    name: 'Building A68',
    childrenId: [
        firstFloorId
    ],
    category: fixtureAssetCategory[0]._id
};

const firstFloorAsset = {
    _id: firstFloorId,
    name: 'First Floor',
    parentId: buildingId,
    childrenId: [
        wallId,
        doorId
    ],
    category: fixtureAssetCategory[0]._id
};

const wallAsset = {
    _id: wallId,
    name: 'Left Black Wall',
    parentId: firstFloorId,
    category: fixtureAssetCategory[0]._id
};

const doorAsset = {
    _id: doorId,
    name: 'Main Door',
    parentId: firstFloorId,
    category: fixtureAssetCategory[0]._id
};

module.exports = [
    buildingAsset,
    firstFloorAsset,
    wallAsset,
    doorAsset
];