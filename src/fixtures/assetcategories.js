const { ObjectID: ObjectId } = require('mongodb');
 

const buildingcatId = new ObjectId(); 
const earthworkId = new  ObjectId();

const buildingCategory = {
    _id: buildingcatId,
    name: 'Building'
};

const earthWorkCategory = {
    _id: earthworkId,
    name: 'Earthwork'
};

module.exports = [
  buildingCategory,
  earthWorkCategory
]