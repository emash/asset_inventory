
# Get started

- [Install MongoDb on your computer](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
- Create a localhost server running on the 27017 port.
- The database used for development is called "db_assets"
- To run the fixtures, install the following package globally: ```bash $ sudo npm install -g node-mongodb-fixtures```
- Load the fixtures with with the command: 
  ```bash $ mongodb-fixtures load -u mongodb://localhost:27017/[DB_NAME] --path ./fixtures/ ```

## ToDo
- Create a .env file for database connection
- Create a database provider file
- Create a postman collection to share between devs

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## License

  Nest is [MIT licensed](LICENSE).
